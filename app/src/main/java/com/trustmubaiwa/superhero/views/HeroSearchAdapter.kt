package com.trustmubaiwa.superhero.views

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.trustmubaiwa.superhero.R
import com.trustmubaiwa.superhero.repository.models.ResultsItem
import kotlin.reflect.KFunction1

class HeroSearchAdapter(
    private val searchResult: List<ResultsItem>,
    val navigationMethod: KFunction1<ResultsItem, Unit>
): RecyclerView.Adapter<HeroSearchAdapter.HeroSearchViewHolder>() {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HeroSearchViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.hero_search_item, parent, false)
        return HeroSearchViewHolder(view)
    }

    override fun onBindViewHolder(holder: HeroSearchViewHolder, position: Int) {
        holder.bind(searchResult[position])
    }

    override fun getItemCount(): Int {
        return searchResult.size
    }

    class HeroSearchViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        val imageView = view.findViewById<ImageView>(R.id.search_item_img)
        val name = view.findViewById<TextView>(R.id.search_item_name)
        val fullname = view.findViewById<TextView>(R.id.search_item_full_name)

        fun bind(item: ResultsItem) {
            Glide.with(view.context).load(item.image.url).into(imageView)
            name.text = item.name
            fullname.text = item.biography.fullName
        }


    }

}