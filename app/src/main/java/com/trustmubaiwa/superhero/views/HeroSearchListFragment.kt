package com.trustmubaiwa.superhero.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.trustmubaiwa.superhero.databinding.FragmentHeroSearchListBinding
import com.trustmubaiwa.superhero.repository.models.ResultsItem
import com.trustmubaiwa.superhero.viewmodels.SearchHeroViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HeroSearchListFragment : Fragment() {

    private var binding: FragmentHeroSearchListBinding? = null
    private val viewModel: SearchHeroViewModel by viewModels()
    private var heroAdapter: HeroSearchAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentHeroSearchListBinding.inflate(inflater)
        binding?.lifecycleOwner = this
        binding?.viewModel = viewModel
        setupUI()
        initialiseObservers()
        return binding?.root
    }

    private fun initialiseObservers() {
        viewModel.getSuccessState().observe(viewLifecycleOwner) {
            if (it) {
                heroAdapter = HeroSearchAdapter(viewModel.getHeroInformationList(), ::navigateToDetailsScreen)
                binding?.searchScreenRecyclerView?.let {
                    it.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                    it.adapter = heroAdapter
                }
            }
        }
    }

    private fun setupUI() {
        binding?.searchScreenSearchButton?.setOnClickListener {
            binding?.searchScreenEditText?.let {
                viewModel.searchHero(searchText = it.text.toString())
            }
        }
    }

    private fun navigateToDetailsScreen(items: ResultsItem) {

    }
}