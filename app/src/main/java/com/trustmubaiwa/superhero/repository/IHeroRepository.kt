package com.trustmubaiwa.superhero.repository

import com.trustmubaiwa.superhero.models.*
import com.trustmubaiwa.superhero.repository.models.HeroInformationEntity
import com.trustmubaiwa.superhero.repository.models.PowerStatsEntity
import com.trustmubaiwa.superhero.repository.models.SearchResultEntity

interface IHeroRepository {

    suspend fun getCharacterInfo(id: String): HeroInformationEntity?

    suspend fun getPowerStats(id: String): PowerStatsEntity?

    suspend fun getCharacterBiography(id: String): CharacterBiography?

    suspend fun getCharacterAppearance(id: String): CharacterAppearance?

    suspend fun getCharacterWork(id: String): CharacterWorkModel?

    suspend fun getCharacterConnections(id: String): CharacterConnection?

    suspend fun getCharacterImage(id: String): CharacterImageModel?

    suspend fun getCharacterSearch(id: String): SearchResultEntity?
}