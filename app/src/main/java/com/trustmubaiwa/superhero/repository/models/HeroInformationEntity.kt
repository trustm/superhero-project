package com.trustmubaiwa.superhero.repository.models


import com.google.gson.annotations.SerializedName


data class HeroInformationEntity(
    @SerializedName("image")
    val image: HeroImage,
    @SerializedName("appearance")
    val appearance: HeroAppearance,
    @SerializedName("response")
    val response: String = "",
    @SerializedName("work")
    val work: HeroWork,
    @SerializedName("name")
    val name: String = "",
    @SerializedName("powerstats")
    val powerstats: HeroPowerstats,
    @SerializedName("id")
    val id: String = "",
    @SerializedName("biography")
    val biography: HeroBiography,
    @SerializedName("connections")
    val connections: HeroConnections
)


data class HeroWork(
    @SerializedName("occupation")
    val occupation: String = "",
    @SerializedName("base")
    val base: String = ""
)


data class HeroBiography(
    @SerializedName("place-of-birth")
    val placeOfBirth: String = "",
    @SerializedName("aliases")
    val aliases: List<String>?,
    @SerializedName("first-appearance")
    val firstAppearance: String = "",
    @SerializedName("publisher")
    val publisher: String = "",
    @SerializedName("alignment")
    val alignment: String = "",
    @SerializedName("full-name")
    val fullName: String = "",
    @SerializedName("alter-egos")
    val alterEgos: String = ""
)


data class HeroPowerstats(
    @SerializedName("strength")
    val strength: String = "",
    @SerializedName("durability")
    val durability: String = "",
    @SerializedName("combat")
    val combat: String = "",
    @SerializedName("power")
    val power: String = "",
    @SerializedName("speed")
    val speed: String = "",
    @SerializedName("intelligence")
    val intelligence: String = ""
)


data class HeroImage(
    @SerializedName("url")
    val url: String = ""
)


data class HeroAppearance(
    @SerializedName("eye-color")
    val eyeColor: String = "",
    @SerializedName("gender")
    val gender: String = "",
    @SerializedName("race")
    val race: String = "",
    @SerializedName("weight")
    val weight: List<String>?,
    @SerializedName("height")
    val height: List<String>?,
    @SerializedName("hair-color")
    val hairColor: String = ""
)

data class HeroConnections(
    @SerializedName("relatives")
    val relatives: String = "",
    @SerializedName("group-affiliation")
    val groupAffiliation: String = ""
)


