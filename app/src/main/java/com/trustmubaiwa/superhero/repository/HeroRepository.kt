package com.trustmubaiwa.superhero.repository

import com.trustmubaiwa.superhero.common.BaseRepository
import com.trustmubaiwa.superhero.common.DataState
import com.trustmubaiwa.superhero.models.*
import com.trustmubaiwa.superhero.repository.models.HeroInformationEntity
import com.trustmubaiwa.superhero.repository.models.PowerStatsEntity
import com.trustmubaiwa.superhero.repository.models.SearchResultEntity
import com.trustmubaiwa.superhero.services.HeroService
import javax.inject.Inject
import javax.inject.Singleton
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

@Singleton
class HeroRepository @Inject constructor(private val service: HeroService) : BaseRepository(), IHeroRepository {

    override suspend fun getCharacterInfo(id: String): HeroInformationEntity? {
        return try {
            safeApiCall { service.getCharacterInfo(id) }
        } catch (e: Exception) {
            throw e
        }
    }

    fun fetchCharacterInfo(id: String): Flow<DataState<HeroInformationEntity>> = flow {
        emit(DataState.Loading)
        try {
            val result = getCharacterInfo(id)
            if (result != null) {
                emit(DataState.Success(result))
            } else {
                emit(DataState.EmptyData)
            }

        } catch (e: Exception) {
            emit(DataState.Error(e))
        }
    }

    override suspend fun getPowerStats(id: String): PowerStatsEntity? {
        return try {
            safeApiCall { service.getPowerStats(id) }
        } catch (e: Exception) {
            throw e
        }
    }

    fun fetchPowerState(id: String): Flow<DataState<PowerStatsEntity>> = flow {
        emit(DataState.Loading)
        try {
            val result = getPowerStats(id)
            if (result != null) {
                emit(DataState.Success(result))
            } else {
                emit(DataState.EmptyData)
            }
        } catch (e: Exception) {
            emit(DataState.Error(e))
        }
    }

    override suspend fun getCharacterBiography(id: String): CharacterBiography? {
        return try {
            safeApiCall { service.getCharacterBiography(id) }
        } catch (e: Exception) {
            throw e
        }
    }

    fun fetchCharacterBiography(id: String): Flow<DataState<CharacterBiography>> = flow {
        emit(DataState.Loading)
        try {
            val result = getCharacterBiography(id)
            if (result != null) {
                emit(DataState.Success(result))
            } else {
                emit(DataState.EmptyData)
            }
        } catch (e: Exception) {
            emit(DataState.Error(e))
        }
    }

    override suspend fun getCharacterAppearance(id: String): CharacterAppearance? {
        return try {
            safeApiCall { service.getCharacterAppearance(id) }
        } catch (e: Exception) {
            throw e
        }
    }

    fun fetchCharacterAppearance(id: String): Flow<DataState<CharacterAppearance>> = flow {
        emit(DataState.Loading)
        try {
            val result = getCharacterAppearance(id)
            if (result != null) {
                emit(DataState.Success(result))
            } else {
                emit(DataState.EmptyData)
            }
        } catch (e: Exception) {
            emit(DataState.Error(e))
        }
    }

    override suspend fun getCharacterWork(id: String): CharacterWorkModel? {
        return try {
            safeApiCall { service.getCharacterWork(id) }
        } catch (e: Exception) {
            throw e
        }
    }

    fun fetchCharacterWork(id: String): Flow<DataState<CharacterWorkModel>> = flow {
        emit(DataState.Loading)
        try {
            val result = getCharacterWork(id)
            if (result != null) {
                emit(DataState.Success(result))
            } else {
                emit(DataState.EmptyData)
            }
        } catch (e: Exception) {
            emit(DataState.Error(e))
        }
    }

    override suspend fun getCharacterConnections(id: String): CharacterConnection? {
        return try {
            safeApiCall { service.getCharacterConnections(id) }
        } catch (e: Exception) {
            throw e
        }
    }

    fun fetchCharacterConnections(id: String): Flow<DataState<CharacterConnection>> = flow {
        emit(DataState.Loading)
        try {
            val result = getCharacterConnections(id)
            if (result != null) {
                emit(DataState.Success(result))
            } else {
                emit(DataState.EmptyData)
            }
        } catch (e: Exception) {
            emit(DataState.Error(e))
        }
    }

    override suspend fun getCharacterImage(id: String): CharacterImageModel? {
        return try {
            safeApiCall { service.getCharacterImage(id) }
        } catch (e: Exception) {
            throw e
        }
    }

    fun fetchCharacterImage(id: String): Flow<DataState<CharacterImageModel>> = flow {
        emit(DataState.Loading)
        try {
            val result = getCharacterImage(id)
            if (result != null) {
                emit(DataState.Success(result))
            } else {
                emit(DataState.EmptyData)
            }
        } catch (e: Exception) {
            emit(DataState.Error(e))
        }
    }

    override suspend fun getCharacterSearch(id: String): SearchResultEntity? {
        return try {
            safeApiCall { service.getCharacterSearch(id) }
        } catch (e: Exception) {
            throw e
        }
    }

    fun fetchCharacterSearch(id: String): Flow<DataState<SearchResultEntity>> = flow {
        emit(DataState.Loading)
        try {
            val result = getCharacterSearch(id)
            if (result != null) {
                emit(DataState.Success(result))
            } else {
                emit(DataState.EmptyData)
            }
        } catch (e: Exception) {
            emit(DataState.Error(e))
        }
    }
}
