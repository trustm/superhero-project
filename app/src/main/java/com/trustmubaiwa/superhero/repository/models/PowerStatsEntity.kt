package com.trustmubaiwa.superhero.repository.models


import com.google.gson.annotations.SerializedName

data class PowerStatsEntity(
    @SerializedName("strength")
    val strength: String = "",
    @SerializedName("response")
    val response: String = "",
    @SerializedName("durability")
    val durability: String = "",
    @SerializedName("name")
    val name: String = "",
    @SerializedName("combat")
    val combat: String = "",
    @SerializedName("id")
    val id: String = "",
    @SerializedName("power")
    val power: String = "",
    @SerializedName("speed")
    val speed: String = "",
    @SerializedName("intelligence")
    val intelligence: String = ""
)


