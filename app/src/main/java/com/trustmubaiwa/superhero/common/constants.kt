package com.trustmubaiwa.superhero.common

@Retention(AnnotationRetention.SOURCE)
annotation class HeroConstants {
    object Api {
        const val BASE_URL = "https://www.superheroapi.com/api/4137222503060019/"
    }
}