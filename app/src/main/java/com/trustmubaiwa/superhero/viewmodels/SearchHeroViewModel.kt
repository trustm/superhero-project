package com.trustmubaiwa.superhero.viewmodels

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.trustmubaiwa.superhero.common.DataState
import com.trustmubaiwa.superhero.repository.HeroRepository
import com.trustmubaiwa.superhero.repository.models.ResultsItem
import com.trustmubaiwa.superhero.repository.models.SearchResultEntity
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class SearchHeroViewModel @Inject constructor(
    val repository: HeroRepository,
) : ViewModel() {
    private val coroutineContext: CoroutineDispatcher = Dispatchers.IO

    private var heroInformationList: List<ResultsItem> = emptyList()
    fun getHeroInformationList() = heroInformationList

    private val _isLoading: MutableLiveData<Boolean> = MutableLiveData(false)
    fun getLoadingState(): LiveData<Boolean> = _isLoading
    private val _isErrorState: MutableLiveData<Boolean> = MutableLiveData(false)
    fun getErrorState(): LiveData<Boolean> = _isErrorState
    private val _isEmptyState: MutableLiveData<Boolean> = MutableLiveData(false)
    fun getEmptyState(): LiveData<Boolean> = _isEmptyState
    private val _isSuccessState: MutableLiveData<Boolean> = MutableLiveData(false)
    fun getSuccessState(): LiveData<Boolean> = _isSuccessState

    fun searchHero(searchText: String) {
        viewModelScope.launch {
            withContext(coroutineContext) {
                repository.fetchCharacterSearch(searchText)
                    .catch {
                        Log.d("NetworkError", "Failed fetching Hero information")
                    }
                    .collect {
                        processNetworkSearchResult(it)
                    }
            }
        }
    }

    private fun processNetworkSearchResult(it: DataState<SearchResultEntity>) {
        when (it) {
            is DataState.Loading -> renderLoadingState()

            is DataState.Success -> renderSuccessState(it.data)

            is DataState.EmptyData -> renderEmptyDataState()

            is DataState.Error -> renderErrorState(it.exception)
        }
    }

    private fun renderErrorState(exception: Exception) {
        _isEmptyState.value = true
        _isLoading.value = false
        _isErrorState.value = false
        _isSuccessState.value = false
    }

    private fun renderEmptyDataState() {
        _isEmptyState.value = true
        _isLoading.value = false
        _isErrorState.value = false
        _isSuccessState.value = false
    }

    private fun renderSuccessState(data: SearchResultEntity) {
        if (!data.results.isNullOrEmpty()) {
            heroInformationList = data.results
        }
        _isSuccessState.value = true
        _isLoading.value = false
        _isErrorState.value = false
        _isEmptyState.value = false
    }

    private fun renderLoadingState() {
        _isLoading.value = true
        _isErrorState.value = false
        _isEmptyState.value = false
        _isSuccessState.value = false
    }

}