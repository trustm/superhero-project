package com.trustmubaiwa.superhero.services

import com.trustmubaiwa.superhero.models.*
import com.trustmubaiwa.superhero.repository.models.HeroInformationEntity
import com.trustmubaiwa.superhero.repository.models.PowerStatsEntity
import com.trustmubaiwa.superhero.repository.models.SearchResultEntity
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface HeroService {

    @GET("{id}")
    suspend fun getCharacterInfo(@Path("id") id: String): Response<HeroInformationEntity>

    @GET("{id}/powerstats")
    suspend fun getPowerStats(@Path("id") id: String): Response<PowerStatsEntity>

    @GET("{id}/biography")
    suspend fun getCharacterBiography(@Path("id") id: String): Response<CharacterBiography>

    @GET("{id}/appearance")
    suspend fun getCharacterAppearance(@Path("id") id: String): Response<CharacterAppearance>

    @GET("{id}/work")
    suspend fun getCharacterWork(@Path("id") id: String): Response<CharacterWorkModel>

    @GET("{id}/connections")
    suspend fun getCharacterConnections(@Path("id") id: String): Response<CharacterConnection>

    @GET("{id}/image")
    suspend fun getCharacterImage(@Path("id") id: String): Response<CharacterImageModel>

    @GET("search/{name}")
    suspend fun getCharacterSearch(@Path("name") name: String): Response<SearchResultEntity>
}