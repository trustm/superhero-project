package com.trustmubaiwa.superhero.models

data class CharacterImageModel(
    val response: String,
    val id: String,
    val name: String,
    val url: String,
)
