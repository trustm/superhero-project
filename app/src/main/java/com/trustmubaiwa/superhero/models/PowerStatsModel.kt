package com.trustmubaiwa.superhero.models

data class PowerStatsModel(
    val combat: String? = "",
    val durability: String? = "",
    val id: String? = "",
    val intelligence: String? = "",
    val name: String? = "",
    val power: String? = "",
    val response: String? = "",
    val speed: String? = "",
    val strength: String? = ""
)