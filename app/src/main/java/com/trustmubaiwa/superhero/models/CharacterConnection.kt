package com.trustmubaiwa.superhero.models

import com.google.gson.annotations.SerializedName

data class CharacterConnection(
    @SerializedName("group-affiliation") val group_affiliation: String,
    @SerializedName("id") val id: String,
    @SerializedName("name") val name: String,
    @SerializedName("relatives") val relatives: String,
    @SerializedName("response") val response: String
)