package com.trustmubaiwa.superhero.models

import com.google.gson.annotations.SerializedName

data class CharacterBiography(
    @SerializedName("aliases") val aliases: List<String>,
    @SerializedName("alignment") val alignment: String,
    @SerializedName("alter-egos") val alter_egos: String,
    @SerializedName("first-appearance") val first_appearance: String,
    @SerializedName("full-name") val full_name: String,
    @SerializedName("id") val id: String,
    @SerializedName("name") val name: String,
    @SerializedName("place-of-birth") val place_of_birth: String,
    @SerializedName("publisher") val publisher: String,
    @SerializedName("response") val response: String
)