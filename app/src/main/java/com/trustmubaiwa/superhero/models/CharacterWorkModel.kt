package com.trustmubaiwa.superhero.models

data class CharacterWorkModel(
    val base: String,
    val id: String,
    val name: String,
    val occupation: String,
    val response: String
)