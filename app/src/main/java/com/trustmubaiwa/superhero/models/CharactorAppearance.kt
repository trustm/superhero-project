package com.trustmubaiwa.superhero.models

import com.google.gson.annotations.SerializedName

data class CharacterAppearance(
    @SerializedName("eye-color") val eye_color: String,
    @SerializedName("gender") val gender: String,
    @SerializedName("hair-color") val hair_color: String,
    @SerializedName("height") val height: List<String>,
    @SerializedName("id") val id: String,
    @SerializedName("name") val name: String,
    @SerializedName("race") val race: String,
    @SerializedName("response") val response: String,
    @SerializedName("weight") val weight: List<String>
)